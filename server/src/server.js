import express from 'express';
import bodyParser from 'body-parser';
import router from "./routes/index";
import cors from 'cors';

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', router);

const port = 8080;

app.listen(port, () => {
  console.log(`'Server started port ${port}'`)
})