const { UserRepository } = require('../repositories/userRepository');

class UserService {

  createUser(userData) {
    let user;
    try {
      user = UserRepository.create(userData);
    }
    catch { }
    finally {
      if (!user) {
        return null;
      }
      return user;
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getUsersList() {
    const usersList = UserRepository.getAll();
    if (!usersList) {
      return null;
    }
    return usersList;
  }

  getUser(id) {
    const user = this.search(id);
    if (!user) {
      return null
    }
    return user;
  }

  removeUser(id) {
    const removedUser = UserRepository.delete(id);
    return removedUser;
  }

  updateUser(id, data) {
    const { email, username } = data;

    if (email) {
      const emailSearch = this.getUser({ email });
      const usernameSeach = this.getUser({ username });

      if (emailSearch && (emailSearch.id !== id)) {
        return null
      } else if (usernameSeach && (usernameSeach.id !== id)) {
        return null
      }
    }

    const updatedUser = UserRepository.update(id, data);
    return updatedUser;
  }

}

module.exports = new UserService();