const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {

  addMessage(messageData) {
    let message;
    try {
      message = MessageRepository.create(messageData);
    }
    catch { }
    finally {
      if (!message) {
        return null;
      }
      return message;
    }
  }

  search(search) {
    const item = MessageRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getMessages() {
    const messages = MessageRepository.getAll();
    if (!messages) {
      return null;
    }
    return messages;
  }

  getMessage(id) {
    const message = this.search(id);
    if (!message) {
      return null
    }
    return message;
  }

  removeMessage(id) {
    const removed = MessageRepository.delete(id);
    return removed;
  }

  updateMessage(id, data) {
    const updated = MessageRepository.update(id, data);
    return updated;
  }

}

module.exports = new MessageService();