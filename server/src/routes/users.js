import { Router } from 'express';
import UserService from '../services/userService';
import { responseMiddleware } from '../middlewares/response.middleware'

const router = Router();

router
  .get('/', (request, response, next) => {
    const users = UserService.getUsersList();
    if (users) {
      response.body = users;
    } else {
      response.err = {
        message: 'Failed to get users list',
        status: 404
      };
    }
    next();
  }, responseMiddleware)
  .post('/', (request, response, next) => {
    const { body } = request;
    const { username } = body;
    const user = UserService.getUser({ username });
    if (user) {
      response.err = {
        status: 400,
        message: 'Username already exist'
      }
    } else {
      const newUser = UserService.createUser(body);
      response.body = newUser;
    }
    next()
  }, responseMiddleware)
  .put('/:id', (request, response, next) => {
    const { id } = request.params;
    const user = UserService.getUser({ id });

    if (!user) {
      response.err = {
        status: 404,
        message: 'User not found'
      }
    } else {
      const { body } = request;
      const updated = UserService.updateUser(id, body);

      if (!updated) {
        response.err = {
          status: 400,
          message: 'Email & username alredy exist'
        }
      } else {
        response.body = updated;
      }
    }
    next();
  }, responseMiddleware)
  .delete('/:id', (request, response, next) => {
    const { id } = request.params;
    const removed = UserService.removeUser(id);

    if (removed.length === 0) {
      response.err = {
        status: 400,
        message: 'Failed to remove'
      }
    } else {
      response.body = {}
    }
    next();
  }, responseMiddleware)

export default router;