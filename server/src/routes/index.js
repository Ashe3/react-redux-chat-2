import { Router } from 'express';

import messageRoutes from './messages';
import userRoutes from './users';
import loginRoutes from './login'

const router = Router();

router.use('/api/messages', messageRoutes);
router.use('/api/users', userRoutes);
router.use('/api/login', loginRoutes);

export default router;