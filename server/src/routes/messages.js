import { request, Router } from 'express';
import { responseMiddleware } from '../middlewares/response.middleware';
import MessageService from '../services/messageService';


const router = Router();

router
  .get('/', (request, response, next) => {
    const messages = MessageService.getMessages();

    if (!messages) {
      response.err = {
        status: 404,
        message: 'Failed to get messages'
      };
    } else {
      response.body = messages;
    }
    next();
  }, responseMiddleware)
  .post('/', (request, response, next) => {
    const { body } = request;

    if (!body.avatar) {
      body.avatar = 'https://upload.wikimedia.org/wikipedia/en/b/b1/Portrait_placeholder.png';
    }

    const message = MessageService.addMessage(body);
    if (!message) {
      response.err = {
        status: 400,
        message: 'Failed to create'
      }
    } else {
      response.body = message;
    }
    next()
  }, responseMiddleware)
  .put('/:id', (request, response, next) => {
    const { id } = request.params;
    const message = MessageService.getMessage({ id });

    if (!message) {
      response.err = {
        status: 404,
        message: 'Message not found'
      }
    } else {
      const { body } = request;
      const updated = MessageService.updateMessage(id, body);

      if (!updated) {
        response.err = {
          status: 400,
          message: 'Failed to update message'
        }
      } else {
        response.body = updated;
      }
    }
    next();
  }, responseMiddleware)
  .delete('/:id', (request, response, next) => {
    const { id } = request.params;
    const removed = MessageService.removeMessage(id);

    if (removed.length === 0) {
      response.err = {
        status: 400,
        message: 'Failed to remove'
      }
    } else {
      response.body = {}
    }
    next();
  }, responseMiddleware)

export default router;