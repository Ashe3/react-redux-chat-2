import { Router } from 'express';
import authService from '../services/authService';
import { responseMiddleware } from '../middlewares/response.middleware'

const router = Router();

router
  .post('/', (request, response, next) => {
    const user = authService.login(request.body);
    if (!user) {
      response.err = {
        status: 404,
        message: 'User not found'
      }
    } else {
      response.body = user;
    }
    next();
  }, responseMiddleware);

export default router;