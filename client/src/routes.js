import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import Chat from './containers/Chat';
import LoginPage from './containers/LoginPage';
import UserList from './containers/UserList';
import UserEditor from './containers/UserEditor';
import MessageEditor from './containers/MessageEditor';

export default (
  <Switch>
    <Route exact path='/'>
      <Redirect to='/login'></Redirect>
    </Route>
    <Route path='/users' component={UserList}/>
    <Route path='/usereditor' component={UserEditor}/>
    <Route path='/login' component={LoginPage} />
    <Route path='/editor' component={MessageEditor} />
    <Route exact path='/chat' component={Chat} />
  </Switch>
);
