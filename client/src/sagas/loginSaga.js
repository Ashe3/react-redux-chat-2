import { takeEvery, call, put, select } from 'redux-saga/effects';
import { loginFailed, loginSuccess } from '../store/actionCreators/loginActionCreators';
import { authorize } from '../helpers/apiHelper';

export function* watchLoginUser() {
  yield takeEvery('LOGIN_USER', fetchLoginAsync)
};

const loginSelect = (state) => state.login;

function* fetchLoginAsync() {

  const { username, password } = yield select(loginSelect);
  const data = yield call(authorize, { username, password })
  if (!data.error) {
    yield put(loginSuccess(data));
  } else {
    const { message } = data;
    yield put(loginFailed({ username, password: '', isLogin: false, message }));
  }
};
