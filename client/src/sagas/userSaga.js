import { select, takeEvery, call, put, all } from "redux-saga/effects";
import { putUserChanges, addNewUser, removeUser, getUsers } from "../helpers/apiHelper";
import { saveUserSuccess, saveUserFailed } from '../store/actionCreators/userActionCreator';
import { fetchUsers } from "../store/actionCreators/usersActionCreators";
import { requestUsers, requestUsersFailed, requestUsersSuccess } from '../store/actionCreators/usersActionCreators';

export function* watchFetchUserChanges() {
  yield all([
    takeEvery('SAVE_USER', saveUserAsync),
    takeEvery('DELETE_USER', removeUserAsync)
  ])
};

const userSelect = (state) => state.user;

function* saveUserAsync() {
  const { user, isEdit } = yield select(userSelect);
  let data;

  if (isEdit) {
    data = yield call(putUser, user)
  } else {
    data = yield call(addUser, user)
  }

  if (data.error) {
    const { message } = data;
    yield put(saveUserFailed({ user, message, isEdit }));
  } else {
    yield put(saveUserSuccess({ message: 'Success', isEdit }));
  };
};

function* putUser(user) {
  const data = yield call(putUserChanges, user);
  return data;
};

function* addUser(user) {
  const data = yield call(addNewUser, user);
  return data;
};

function* removeUserAsync() {
  const user = yield select(userSelect);
  yield call(removeUser, user);
  yield put(fetchUsers());
}

export function* watchFetchUser() {
  yield takeEvery('FETCH_USERS', fetchUsersAsync)
};

function* fetchUsersAsync() {
  yield put(requestUsers());

  const users = yield call(getUsers);

  if (!users) {
    yield put(requestUsersFailed());
  } else {
    yield put(requestUsersSuccess(users));
  }
};
