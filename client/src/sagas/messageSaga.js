import { select, takeEvery, call, put, all } from "redux-saga/effects";
import { getMessages, putMessage, addNewMessage, deleteMessage } from '../helpers/apiHelper';
import {
  fetchMessages,
  requestMessages, requestMessagesFailed, requestMessagesSuccess,
  saveMessageFailed, saveMessageSuccess,
  removeMessageSuccess, removeMessageFailed
} from '../store/actionCreators/messageActionCreators'

export function* watchFetchMessages() {
  yield all([
    takeEvery('FETCH_MESSAGES', fetchMessagesAsync),
    takeEvery('SAVE_MESSAGE', saveMessageAsync),
    takeEvery('REMOVE_MESSAGE', removeMessageAsync)
  ]);
};

const messageSelect = (state) => state.message;

function* fetchMessagesAsync() {
  yield put(requestMessages());

  const data = yield call(getMessages)

  if (data.error) {
    yield put(requestMessagesFailed(data.message));
  } else {
    yield put(requestMessagesSuccess(data));
  }
};

function* saveMessageAsync() {
  const { data: message, isEdit } = yield select(messageSelect);
  let data;

  if (isEdit) {
    data = yield call(editMessage, message);
  } else {
    data = yield call(addMessage, message);
  }

  if (data.error) {
    yield put(saveMessageFailed(message, 'Failed to save message', isEdit));
  } else {
    yield put(saveMessageSuccess(message, isEdit));
    if (!isEdit) {
      yield put(fetchMessages())
    }
  };
};

function* editMessage(user) {
  const data = yield call(putMessage, user);
  return data;
};

function* addMessage(user) {
  const data = yield call(addNewMessage, user);
  return data;
};

function* removeMessageAsync() {
  const { data: message } = yield select(messageSelect);
  
  const data = yield call(deleteMessage, message);

  if (!data.error) {
    yield put(removeMessageSuccess(message));
  } else {
    yield put(removeMessageFailed(data.message));
  }

  yield put(fetchMessages())
}