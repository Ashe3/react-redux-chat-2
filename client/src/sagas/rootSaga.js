import { all } from 'redux-saga/effects'

import { watchFetchUser, watchFetchUserChanges } from './userSaga';
import { watchLoginUser } from './loginSaga';
import { watchFetchMessages } from './messageSaga'

export default function* rootSaga() {
  yield all([
    watchFetchUser(),
    watchLoginUser(),
    watchFetchUserChanges(),
    watchFetchMessages()
  ]);
};