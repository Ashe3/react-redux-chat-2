import React from 'react';
import { Link } from 'react-router-dom';

const AdminPanel = () => {
  return (
    <div>
      <span>Admin panel</span>
      <div className='sendBtn width99'><Link className='center' to="/users">User List</Link></div>
    </div>
  );
}

export default AdminPanel;