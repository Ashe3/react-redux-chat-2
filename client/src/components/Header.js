import React from 'react';

const Header = (props) => {

  const formatDate = () => {
    if (props.lastMsg) {
      const date = `${new Date(props.lastMsg.createdAt).getHours()}:${new Date(props.lastMsg.createdAt).getMinutes()}`;
      return date;
    }
    return 0;
  }

  return (
    <div className='header'>
      <span>My Chat</span>
      <span>{props.participants} participants</span>
      <span>{props.messages} messages</span>
      <span>Last Message at {formatDate()}</span>
      <span className='exit' onClick={props.logout}>X</span>
    </div>
  );
};

export default Header;