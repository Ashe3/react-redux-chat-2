import React, { useCallback } from 'react';
import { connect } from 'react-redux';

import MsgBox from './Message';
import UserMessage from './UserMessage';
import { sortMessagesByDate } from '../helpers/arrayHandleHelper';

const MsgList = ({ messages, curUserId }) => {

  const messagesEnd = useCallback(node => {
    if (node !== null) {
      node.scrollIntoView({ behavior: "auto" });
    }
  }, []);

  const loadMessages = () => {
    const messageList = sortMessagesByDate(messages).map(({ id, userId, avatar, user, text, createdAt, editedAt }, idx) => {
      switch (userId) {
        case curUserId:
          return <UserMessage
            key={id + idx}
            id={id}
            idx={idx}
            userId={userId}
            text={text}
            createdAt={createdAt}
            editedAt={editedAt} />
        default:
          return <MsgBox
            key={id + idx}
            id={id}
            avatar={avatar}
            userId={userId}
            username={user}
            text={text}
            createdAt={createdAt}
            editedAt={editedAt} />
      }
    });

    return messageList;
  };

  return (
    <div className='message-list'>
      {loadMessages()}
      <div style={{ float: "left", clear: "both", opacity: 0 }}
        ref={messagesEnd}>
      </div>
    </div>
  );

}

const mapStateToProps = (state) => {
  return {
    messages: state.messages.data,
    curUserId: state.login.id
  };
};

export default connect(mapStateToProps)(MsgList);