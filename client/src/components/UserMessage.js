import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { editMessage, removeMessage } from '../store/actionCreators/messageActionCreators';

const UserMessage = ({ text, id, editedAt, createdAt, edit, message, remove, failureMessage }) => {

  const normalizeDate = (date) => {
    return new Date(date).toLocaleString('ru-RU');
  }

  return (
    <div className="message user-message">
      <div className='container'>
        <div className='edit'>
          <span className="material-icons" onClick={() => { edit({ id, text }, true) }}>
            <Link className='white' to='/editor'>create</Link>
          </span>
          <span className="material-icons" onClick={() => { remove({ id }) }}>
            delete
        </span>
        </div>
        <div className='text'>
          <span>{text}</span>
          <span className='date'>{editedAt && editedAt.length ? `Edited ${normalizeDate(editedAt)}` : normalizeDate(createdAt)}</span>
          <span className='small red'>{failureMessage ? failureMessage : null}</span>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    message: state.message.data,
    failureMessage: state.message.failureMessage
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    edit: (message, isEdit) => dispatch(editMessage(message, isEdit)),
    remove: (message) => dispatch(removeMessage(message))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserMessage);