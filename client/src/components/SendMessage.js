import React, { useState } from 'react';
import { connect } from 'react-redux';
import { editMessage, saveMessage } from '../store/actionCreators/messageActionCreators';
import { lastUserMessage } from '../helpers/arrayHandleHelper';
import { useHistory } from 'react-router';

const SendMessage = ({ userId, username, messages, saveMessage, edit }) => {

  const history = useHistory()

  const [value, setValue] = useState('');

  const onChange = (event) => {
    const { value } = event.target;
    setValue(value);
  };

  const onSend = () => {
    const newMessage = getMessageDetails();
    if (newMessage.text.length !== 0) {
      saveMessage(newMessage, false);
      setValue('');
    }
  };

  const handlePressButton = (event) => {
    if (event.type === 'keyup') {
      if (event.key === 'Enter') {
        onSend();
      }

      if (event.key === 'ArrowUp') {
        const lastMessage = lastUserMessage(messages, userId);
        if (lastMessage) {
          edit(lastMessage);
          history.push('/editor')
        }
      }
    }
  };

  const getMessageDetails = () => {
    return {
      user: username,
      text: value,
      userId
    };
  };

  return (
    <div className='send' onKeyPress={handlePressButton}>
      <input
        type='text'
        className='send-field'
        value={value}
        onChange={onChange}
        onKeyUp={handlePressButton}
        placeholder='message here or click ^ to edit' />
      <div className='sendBtn center' onClick={onSend}>Send</div>
    </div>
  );
};

const mapStateToprops = (state) => {
  return {
    userId: state.login.id,
    username: state.login.username,
    messages: state.messages.data
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveMessage: (message, isEdit) => dispatch(saveMessage(message, isEdit)),
    edit: (message) => dispatch(editMessage(message))
  }
};

export default connect(mapStateToprops, mapDispatchToProps)(SendMessage);