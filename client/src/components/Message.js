import React, { useState } from 'react';

const MsgBox = (props) => {

  const [red, setRed] = useState(false);

  const normalizeDate = (date) => {
    return new Date(date).toLocaleString('ru-RU');
  };

  const likeMessage = (event) => {
    setRed(!red)
  };

  return (
    <div className='message'>
      <img src={props.avatar} />
      <div className='text'>
        <span className='name'>{props.username}</span>
        <span>{props.text}</span>
        <span className='date'>{props.editedAt && props.editedAt.length > 0 ? `Edited ${normalizeDate(props.editedAt)}` : normalizeDate(props.createdAt)}</span>
      </div>
      <div id='like' className={red ? 'red' : 'white'} onClick={likeMessage}>
        <span className="material-icons">
          favorite
        </span>
      </div>
    </div>
  );
}

export default MsgBox;