import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { saveMessage } from '../store/actionCreators/messageActionCreators';

const MessageEditor = ({ message, isLogin, save }) => {

  const [text, setText] = useState(message.text);

  const loaded = () => {
    return (
      <div>
        <div className='head1 center'>
          <div className='center white size30'>Message Editor</div>
          <div className='sendBtn'><Link className='center' to='/chat'>Back to chat</Link></div>
        </div>
        <textarea rows='10' cols='100' autoFocus value={text} onChange={(e) => { setText(e.target.value) }}></textarea>
        <div className='btns'>
          <div className='btn edit-user' onClick={() => save({ text, id: message.id }, true)}><Link to='/chat'>save</Link></div>
          <div className='btn delete'><Link className='white' to='/chat'>cancel</Link></div>
        </div>
      </div>
    );
  };

  return (
    <div>{!isLogin ? <Redirect to='/login' /> : loaded()}</div>
  )
};

const mapStateToprops = (state) => {
  return {
    message: state.message.data,
    isLogin: state.login.isLogin
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    save: (message, isEdit) => dispatch(saveMessage(message, isEdit))
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(MessageEditor)