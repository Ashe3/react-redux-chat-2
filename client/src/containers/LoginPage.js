import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { loginUser, saveLogin } from '../store/actionCreators/loginActionCreators';

import './styles/Login.css'

const LoginPage = ({ save, login, isLogin, isFailed, failedMessage, isAdmin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const tryLogin = () => {
    save({ username, password });
    login();
  }

  const renderLogin = () => {
    return (
      <div className='login-field'>
        {isFailed ? <span>{failedMessage}</span> : null}
        <input type='text' placeholder='username' value={username} onChange={(e) => { setUsername(e.target.value) }} />
        <input type='password' placeholder='password' value={password} onChange={(e) => setPassword(e.target.value)} />
        <div className='sendBtn sendl' onClick={tryLogin}>Login</div>
      </div>
    )
  };

  return (
    <div>{!isLogin ? renderLogin() : isAdmin == 'true' ? <Redirect to='/users' /> : <Redirect to='/chat' />}</div>
  )
};

const mapStateToProps = (state) => {
  return {
    isLogin: state.login.isLogin,
    isFailed: state.login.failed,
    failedMessage: state.login.message,
    isAdmin: state.login.isAdmin
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    save: (userData) => dispatch(saveLogin(userData)),
    login: () => dispatch(loginUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);