import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, useHistory } from 'react-router-dom';
import { saveUser } from '../store/actionCreators/userActionCreator';

const UserEditor = ({ isEdit, user, save, message, admin, isFailed }) => {


  ///fix this
  useEffect(() => {
    if (!isFailed) {
      history.push('/users');
    }
  });

  const [username, setUsername] = useState(user.username);
  const [password, setPassword] = useState(user.password);
  const [email, setEmail] = useState(user.email);
  const [adminRights, setAdminRights] = useState(false);
  const history = useHistory();

  const handleSave = () => save({ user: { ...user, username, password, email, isAdmin: adminRights }, isEdit });

  const loaded = () => {

    return (
      <div>
        <div className='head1 center'>
          <div className='center white size30'>UserEditor</div>
          <div className='center white'>{message}</div>
          <div className='sendBtn center'><Link to='/chat'>Back to chat</Link></div>
        </div>
        <div className='userEditor'>
          <span>
            <label htmlFor='username'>Username:</label>
          </span>
          <input
            type='text'
            placeholder='username'
            id='username'
            value={username || ''}
            onChange={(e) => setUsername(e.target.value)}/>
          <span>
            <label htmlFor='password'>Password:</label>
          </span>
          <input
            type='password'
            placeholder='password'
            id='password'
            value={password || ''}
            onChange={(e) => setPassword(e.target.value)} />
          <span>
            <label htmlFor='email'>Email:</label>
          </span>
          <input
            type='text'
            placeholder='email'
            id='email'
            value={email || ''}
            onChange={(e) => setEmail(e.target.value)} />
          <span>
            <label htmlFor='isadmin'>Admin:</label>
          </span>
          <input
            type='checkbox'
            id='isadmin'
            checked={adminRights}
            onChange={() => setAdminRights(!adminRights)} />
          <div></div>
          <div className='btns'>
            <div className='btn edit-user' onClick={handleSave}>save</div>
            <div className='btn delete'><Link to='/users'>cancel</Link></div>
          </div>
        </div>
      </div>
    );
  };



  return (
    <div>{!admin ? <Redirect to='/chat' /> : loaded()}</div>
  );

};

const mapStateToProps = (state) => {
  return {
    isEdit: state.user.isEdit,
    user: state.user.user,
    message: state.user.message,
    admin: state.login.isAdmin,
    isFailed: state.user.failed
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    save: (user) => dispatch(saveUser(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);