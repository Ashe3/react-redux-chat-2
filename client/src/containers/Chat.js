import './styles/Chat.css';

import { useEffect } from 'react';
import { connect } from 'react-redux';

import Header from '../components/Header';
import MsgList from '../components/Messages';
import SendMessage from '../components/SendMessage';
import Spinner from '../components/Spinner';
import AdminPanel from '../components/AdminPanel';

import { fetchMessages } from '../store/actionCreators/messageActionCreators';
import { getParticipants, last } from '../helpers/arrayHandleHelper';
import { Redirect } from 'react-router-dom';
import { logoutUser } from '../store/actionCreators/loginActionCreators';


function Chat({ get, messageCount, participants, lastMessage, isLogin, isAdmin, logout, isLoading }) {
  useEffect(() => {
    get();
  }, []);

  const loaded = () => {
    return (
      <div>
        <div className='App'>
          <Header participants={participants} messages={messageCount} lastMsg={lastMessage} logout={logout}/>
          <MsgList />
          <SendMessage />
          {isAdmin === 'true' ? <AdminPanel /> : null}
        </div>
      </div>
    );
  };

  return (
    <div>
      {!isLogin ? <Redirect to='/login' /> : isLoading ? <Spinner /> : loaded()}
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    messages: state.messages.data,
    messageCount: state.messages.data.length,
    participants: getParticipants(state.messages.data),
    lastMessage: last(state.messages.data),
    isLogin: state.login.isLogin,
    isAdmin: state.login.isAdmin,
    isLoading: state.messages.loading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    get: () => dispatch(fetchMessages()),
    logout: () => dispatch(logoutUser())
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Chat);
