import React, { useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchUsers } from '../store/actionCreators/usersActionCreators';
import Spinner from '../components/Spinner';
import './styles/UserList.css'
import { createUser, editUser, removeUser } from '../store/actionCreators/userActionCreator';

const UserList = ({ isLoading, isFailed, admin, users, load, add, edit, remove }) => {

  useEffect(() => {
    load();
  }, []);

  const handleEdit = (event) => {
    const { id } = event.target;
    edit(users[id]);
  }

  const handleDelete = (e) => {
    const { id } = e.target;
    remove(users[id]);
  }

  const loaded = () => {

    const renderUsers = () => {
      return users.map(({ id, username, isAdmin, password, email }, idx) => {
        return (
          <div key={idx}>
            <span>{id}</span>
            <span>{username}</span>
            <span type='password'>{password}</span>
            <span>{email}</span>
            <span>{isAdmin === 'true' ? 'admin' : 'user'}</span>
            <div className='btn edit-user' onClick={handleEdit}><Link to='/usereditor' id={idx}>Edit</Link></div>
            <div className='btn delete' id={idx} onClick={handleDelete}>Delete</div>
          </div>
        );
      });
    };

    return (
      <div>
        <div className='head1 center'>
          <div className='center white size30'>Users List</div>
          <div className='sendBtn'><Link className='center' to='/chat'>Back to chat</Link></div>
          <div className='sendBtn' onClick={add}><Link className='center' to='/usereditor'>Add user</Link></div>
        </div>
        <div className='user-list'>
          {isFailed ? <div className='size30 center'>Failed to fetch users</div> : renderUsers()}
        </div>
      </div>
    )
  };

  return (
    <div>{isLoading ? <Spinner /> : !admin ? <Redirect to='/chat' /> : loaded()}</div>
  );


}

const mapStateToProps = (state) => {
  return {
    isLoading: state.users.loading,
    isFailed: state.users.failed,
    users: state.users.userList,
    admin: state.login.isAdmin
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    load: () => dispatch(fetchUsers()),
    edit: (user) => dispatch(editUser(user)),
    add: () => dispatch(createUser()),
    remove: (user) => dispatch(removeUser(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);