export const CREATE_USER = 'CREATE_USER';
export const EDIT_USER = 'EDIT_USER';
export const SAVE_USER = 'SAVE_USER';
export const SAVE_USER_SUCCESS = 'SAVE_USER_SUCCESS';
export const SAVE_USER_FAILED = 'SAVE_USER_FAILED';
export const DELETE_USER = 'DELETE_USER';