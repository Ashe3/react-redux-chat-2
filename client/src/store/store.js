
import { applyMiddleware, createStore, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import initialState from './initialState';
import reducers from './reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';

const sageMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, initialState, composeEnhancers(applyMiddleware(sageMiddleware)));
sageMiddleware.run(rootSaga)


export default store;