import {
  FETCH_MESSAGES,
  REQUEST_MESSAGES, REQUEST_MESSAGES_FAILED, REQUEST_MESSAGES_SUCCESS,
  SAVE_MESSAGE, SAVE_MESSAGE_FAILED, SAVE_MESSAGE_SUCCESS,
  EDIT_MESSAGE,
  REMOVE_MESSAGE, REMOVE_MESSAGE_FAILED, REMOVE_MESSAGE_SUCCESS
} from '../actions/messageActions'

export const fetchMessages = () => {
  return {
    type: FETCH_MESSAGES,
  };
};

export const requestMessages = () => {
  return {
    type: REQUEST_MESSAGES,
    value: { data: [], loading: true, failed: false }
  };
};

export const requestMessagesSuccess = (messages) => {
  return {
    type: REQUEST_MESSAGES_SUCCESS,
    value: { data: messages, loading: false, failed: false }
  };
};

export const requestMessagesFailed = (failureMessage) => {
  return {
    type: REQUEST_MESSAGES_FAILED,
    value: { data: [], loading: false, failed: true, failureMessage }
  };
};

export const saveMessage = (message, isEdit) => {
  return {
    type: SAVE_MESSAGE,
    value: { data: message, isEdit: isEdit, loading: true, failed: false }
  };
};

export const editMessage = (message, isEdit) => {
  return {
    type: EDIT_MESSAGE,
    value: { data: message, isEdit }
  };
};

export const saveMessageFailed = (message, failureMessage, isEdit) => {
  return {
    type: SAVE_MESSAGE_FAILED,
    value: { data: message, isEdit, loading: true, failed: true, failureMessage }
  };
};

export const saveMessageSuccess = (message, isEdit) => {
  return {
    type: SAVE_MESSAGE_SUCCESS,
    value: { data: message, isEdit, loading: false, failed: false }
  };
};

export const removeMessage = (message) => {
  return {
    type: REMOVE_MESSAGE,
    value: { data: message, loading: true, failed: false }
  };
};

export const removeMessageFailed = (messageData, failureMessage) => {
  return {
    type: REMOVE_MESSAGE_FAILED,
    value: { data: messageData, loading: false, failed: true, failureMessage }
  };
};

export const removeMessageSuccess = (message) => {
  return {
    type: REMOVE_MESSAGE_SUCCESS,
    value: { data: message, loading: false, failed: false }
  };
};
