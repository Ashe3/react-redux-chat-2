import { LOGIN_USER, LOGIN_USER_FAILED, LOGIN_USER_SUCCESS, LOGOUT_USER, SAVE_LOGIN } from "../actions/loginActions"

export const loginUser = () => {
  return {
    type: LOGIN_USER,
  };
};

export const saveLogin = (userData) => {
  return {
    type: SAVE_LOGIN,
    value: userData
  };
};

export const logoutUser = () => {
  return {
    type: LOGOUT_USER,
    value: { username: '', password: '', isLogin: false, failed: false }
  };
};

export const loginSuccess = (userData) => {
  return {
    type: LOGIN_USER_SUCCESS,
    value: { ...userData, failed: false, isLogin: true }
  };
};

export const loginFailed = (userData) => {
  return {
    type: LOGIN_USER_FAILED,
    value: { ...userData, failed: true }
  };
};