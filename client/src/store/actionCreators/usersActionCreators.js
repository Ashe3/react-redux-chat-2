import { FETCH_USERS, REQUEST_USERS, REQUEST_USERS_FAILED, REQUEST_USERS_SUCCESS } from "../actions/userslistActions"

export const fetchUsers = () => {
  return {
    type: FETCH_USERS,
  };
};

export const requestUsers = () => {
  return {
    type: REQUEST_USERS,
    value: {
      userList: [],
      failed: false,
      loading: true
    }
  };
};

export const requestUsersFailed = () => {
  return {
    type: REQUEST_USERS_FAILED,
    value: {
      userList: [],
      failed: true,
      loading: false
    }
  };
};


export const requestUsersSuccess = (data) => {
  return {
    type: REQUEST_USERS_SUCCESS,
    value: {
      userList: [...data],
      failed: false,
      loading: false
    }
  };
};