import { CREATE_USER, EDIT_USER, SAVE_USER_FAILED, SAVE_USER_SUCCESS, SAVE_USER, DELETE_USER } from "../actions/userActions"

export const createUser = () => {
  return {
    type: CREATE_USER,
    value: { user: {}, isEdit: false, failed: true }
  };
};

export const editUser = (user) => {
  return {
    type: EDIT_USER,
    value: { user, isEdit: true, failed: true }
  };
};

export const saveUser = (userData) => {
  return {
    type: SAVE_USER,
    value: { ...userData, loading: true, failed: true }
  };
};

export const saveUserSuccess = (message) => {
  return {
    type: SAVE_USER_SUCCESS,
    value: { user: {}, loading: false, failed: false, ...message }
  };
};

export const saveUserFailed = (message) => {
  return {
    type: SAVE_USER_FAILED,
    value: { user: {}, loading: false, failed: true, ...message }
  };
};

export const removeUser = (user) => {
  return {
    type: DELETE_USER,
    value: { ...user }
  };
};