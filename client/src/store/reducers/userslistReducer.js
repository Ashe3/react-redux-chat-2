import { REQUEST_USERS, REQUEST_USERS_FAILED, REQUEST_USERS_SUCCESS } from "../actions/userslistActions";

export const usersReducer = (state = [], action) => {
  switch (action.type) {
    case REQUEST_USERS: return action.value;
    case REQUEST_USERS_FAILED: return action.value;
    case REQUEST_USERS_SUCCESS: return action.value;

    default: return state;
  };
};