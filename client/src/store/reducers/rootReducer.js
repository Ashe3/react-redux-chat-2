import { combineReducers } from 'redux';

import { messagesReducer as messages, messageReducer as message } from './messageReducer';
import { loginReducer as login } from './loginReducer';
import { usersReducer as users } from './userslistReducer';
import { userReducer as user } from './userReducer';

const reducers = combineReducers({ messages, message, login, users, user });

export default reducers;