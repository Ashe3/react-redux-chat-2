import { EDIT_MESSAGE, REMOVE_MESSAGE, REQUEST_MESSAGES, REQUEST_MESSAGES_FAILED, REQUEST_MESSAGES_SUCCESS, SAVE_MESSAGE, SAVE_MESSAGE_FAILED, SAVE_MESSAGE_SUCCESS, UPDATE_MESSAGE } from '../actions/messageActions';

export const messagesReducer = (state = {}, action) => {
  switch (action.type) {
    case REQUEST_MESSAGES: return { ...action.value }
    case REQUEST_MESSAGES_SUCCESS: return { ...action.value }
    case REQUEST_MESSAGES_FAILED: return { ...action.value }

    default: return state;
  };
};

export const messageReducer = (state = {}, action) => {
  switch (action.type) {
    case SAVE_MESSAGE: return { ...action.value };
    case SAVE_MESSAGE_SUCCESS: return { ...action.value };
    case SAVE_MESSAGE_FAILED: return { ...action.value };
    case EDIT_MESSAGE: return { ...action.value };
    case REMOVE_MESSAGE: return { ...action.value };

    default: return state;
  };
};