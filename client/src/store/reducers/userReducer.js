import { CREATE_USER, DELETE_USER, EDIT_USER, SAVE_USER, SAVE_USER_FAILED, SAVE_USER_SUCCESS } from "../actions/userActions"

export const userReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_USER: return { ...action.value };
    case EDIT_USER: return { ...action.value };
    case SAVE_USER: return { ...action.value };
    case SAVE_USER_SUCCESS: return { ...action.value };
    case SAVE_USER_FAILED: return { ...action.value };
    case DELETE_USER: return { ...action.value };

    default: return state;
  };
};