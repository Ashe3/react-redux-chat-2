import { LOGIN_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILED, LOGOUT_USER, SAVE_LOGIN } from "../actions/loginActions"

export const loginReducer = (state = {}, action) => {
  switch (action.type) {
    case LOGIN_REQUEST: return state;
    case SAVE_LOGIN: return { ...state, ...action.value };
    case LOGIN_USER_SUCCESS: return { ...action.value };
    case LOGIN_USER_FAILED: return { ...action.value };
    case LOGOUT_USER: return { ...action.value };

    default: return state;
  };
};