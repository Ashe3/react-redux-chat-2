const initialState = {
  messages: {
    data: [],
  },
  message: {
    isEdit: false,
    data: {}
  },
  login: {
    username: '',
    password: '',
    isLogin: false,
    failed: false
  },
  users: {
    userList: [],
    failed: false,
    loading: false
  },
  user: {
    isEdit: false,
    user: {
      id: '',
      username: '',
      password: '',
      email: '',
      isAdmin: false
    }
  }
};

export default initialState;