const apiHelper = (endpoint, opt) => {
  const URL = 'http://localhost:8080';
  const options = {
    ...opt
  };

  return fetch(URL + endpoint, options)
    .then(response => response.json())
    .then(response => {
      if (response.error) {
        return response;
      }
      return response;
    })
};

export const authorize = async (user) => {
  const endpoint = '/api/login';
  const options = {
    method: 'POST',
    body: new URLSearchParams(user),
  };

  const userData = await apiHelper(endpoint, options);

  if (userData) {
    return userData
  }
  return false;
};

export const getUsers = async () => {
  const endpoint = '/api/users';

  const options = {
    method: 'GET',
  };

  return await apiHelper(endpoint, options);
};

export const putUserChanges = async (user) => {
  const { id } = user;
  const endpoint = `/api/users/${id}`;

  const options = {
    method: 'PUT',
    body: new URLSearchParams(user),
  };

  return await apiHelper(endpoint, options);
};

export const addNewUser = async (user) => {
  const endpoint = '/api/users';

  const options = {
    method: 'POST',
    body: new URLSearchParams(user),
  };

  return await apiHelper(endpoint, options);
};

export const removeUser = async ({ id }) => {
  const endpoint = `/api/users/${id}`;

  const options = {
    method: 'DELETE',
  };

  return await apiHelper(endpoint, options);
};

export const getMessages = async () => {
  const endpoint = '/api/messages';
  const options = { method: 'GET' };

  return await apiHelper(endpoint, options);
};

export const putMessage = async (data) => {
  const {id} = data;
  const endpoint = `/api/messages/${id}`;
  const options = {
    method: 'PUT',
    body: new URLSearchParams(data),
  };

  return await apiHelper(endpoint, options);
};

export const addNewMessage = async (data) => {
  const endpoint = '/api/messages';

  const options = {
    method: 'POST',
    body: new URLSearchParams(data),
  };

  return await apiHelper(endpoint, options);
};

export const deleteMessage = async ({ id }) => {
  const endpoint = `/api/messages/${id}`;

  const options = {
    method: 'DELETE',
  };

  return await apiHelper(endpoint, options);
};