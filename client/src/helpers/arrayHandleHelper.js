export const findMessagePosition = (msgId, messages) => {
  return messages.reduce((acc, { id }, index) => {
    if (id === msgId) acc = index;
    return acc;
  }, -1);
};

export const sortMessagesByDate = (data) => {
  const messages = data.sort(({ createdAt: create1 }, { createdAt: create2 }) => new Date(create1) - new Date(create2));
  return messages;
};

export const getParticipants = (messages) => new Set(messages.map(({ userId }) => userId)).size;

export const last = (arr) => arr[arr.length - 1];

export const lastUserMessage = (messageList, id) => {
  const messages = sortMessagesByDate(messageList.filter(({userId}) => userId === id));
  return last(messages) || null;
};